#include <stdio.h>

int main(){
    int numero,un,de,ce,mi;
    printf("ingrese un numero entre 0-9999\n");
    scanf("%d", &numero);

    mi=numero/1000;
    ce=(numero%1000)/100;
    de=(numero%100)/10;
    un=(numero%10);
    printf("unidad %d\n", un);
    printf("decena %d\n", de);
    printf("centena %d\n",ce);
    printf("mil %d\n",mi);
    return 0;
}