#include <stdio.h>
#include <limits.h>
int main() 
{
    // enteros
    printf("tipo             tamaño \t minimo \t maximo\n");
    printf("char               %lu \t \t%ld \t\t  %ld\n", sizeof(char), CHAR_MIN, CHAR_MAX);
    printf("uchar              %lu \t \t%ld \t\t  %ld\n", sizeof(char), 0, UCHAR_MAX);
    printf("short              %lu \t \t%d \t\t   %d\n", sizeof(short), SHRT_MAX,SHRT_MIN);
    printf("ushort             %lu \t \t%d \t\t   %d\n", sizeof(unsigned short), 0,USHRT_MAX);
    printf("int                %lu \t \t%d \t   %d\n", sizeof(int), INT_MAX, INT_MIN);
    printf("uint               %lu \t \t%d \t\t   %d\n", sizeof(unsigned int), 0, INT_MAX);
    printf("long               %lu \t \t%d \t   %d\n", sizeof(long), LONG_MAX, LONG_MIN);
    printf("long long          %lu \t \t%lld \t   %lld\n", sizeof(long long), LLONG_MAX, LLONG_MIN);
    
    //punto flotantes
    printf("float              %lu \t \t%e \t   %e\n", sizeof(float));
    printf("double             %lu\n", sizeof(double));
    printf("long double       %lu\n", sizeof(long double));

}