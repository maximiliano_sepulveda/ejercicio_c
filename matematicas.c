#include <stdio.h>
#include <math.h>

int main()
{
    printf("PI: %f\n", M_PI);
    printf("PI/2 %f\n", M_PI_2);
    printf("PI/4 %f\n", M_PI_4);
    printf("1/PI %f\n", M_1_PI);
    return 0;
}