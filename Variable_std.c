#include <stdio.h>
#include <stdint.h>

int main(){

    int8_t a;
    int16_t b;
    int32_t c;
    int64_t d;

    printf("a: %d, b: %d, c: %d, d: %d ", sizeof(a), sizeof(b), sizeof(c), sizeof(d));
    return 0;
}